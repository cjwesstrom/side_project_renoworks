<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('user_type');
            $table->boolean('design_consultation');
            $table->string('order');
            $table->string('package');
            $table->integer('additional_applied_options');
            $table->decimal('price', 10, 2);
            $table->decimal('gst', 10, 2);
            $table->decimal('credit_applied', 10, 2);
            $table->string('promo_code');
            $table->foreign('project_photos_id')->references('id')->on('project_photos');
            $table->boolean('roofing');
            $table->boolean('trim');
            $table->boolean('siding');
            $table->boolean('stone/brick');
            $table->boolean('windows');
            $table->boolean('shutters');
            $table->boolean('doors');
            $table->boolean('garage doors');
            $table->boolean('railings/columns');
            $table->string('roofing_instructions');
            $table->string('trim_instructions');
            $table->string('siding_instructions');
            $table->string('stone/brick_instructions');
            $table->string('windows_instructions');
            $table->string('shutters_instructions');
            $table->string('doors_instructions');
            $table->string('garage doors_instructions');
            $table->string('railings/columns_instructions');
            $table->string('remodel_reason');
            $table->string('color_palette_preferences');
            $table->string('additional_details');
            )
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');

          // delete connected photos on user deletion
          $table->foreign('project_photos_id')->references('id')->on('project_photos')->onDelete('cascade');
    }
}
